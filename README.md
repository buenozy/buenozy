<h2><strong>Olá! Me chamo Fernando Bueno, sejam bem vindos ao meu perfil.</strong></h2>

<ul>
  <li>🤓 Dev em Desenvolvimento 🤓</li>
</ul>  

---

<ul>
  <li>😷 Atualmente eu trabalho como Técnico de Enfermagem</li>
  <li>🧐 Cursando Análise e Desenvolvimento de Sistemas na <a href="https://www.cruzeirodosulvirtual.com.br/graduacao/analise-e-desenvolvimento-de-sistemas/" target="_blank">UNICSUL</a></li>
  <li>🤯 Estudando Banco de Dados.</li>
  <li>⚔  Meu perfil no <a href="https://www.codewars.com/users/codebuenozy" target="_blank">Codewars</a> 👈</li>
  <li>🖊  Meu perfil no <a href="https://codepen.io/codebuenozy" target="_blank">CodePen</a> 👈</li>
  <li>🦊 Meu perfil no <a href="https://gitlab.com/codebuenozy" target="_blank">GitLab</a> 👈</li> 
  <li>📫 Contate-me no email: nando_silverz@hotmail.com</li>
  <li>💻 Inscreva-se para cursos <strong>GRATUITOS</strong> na área Tech => <a href="https://dio.me/sign-up?ref=ZLPADVQB4Q" target="_blank">DIO</a> 👈</li>
  <li>💻 Matricule-se para cursos na área Tech => <a href="https://cursos.alura.com.br/" target="_blank">Alura</a> 👈</li>
  <li>🌐 Desconto na => <a href="https://hostinger.com.br?REFERRALCODE=1FERNANDO745" target="_blank">Hostinger</a> 👈</li> 
</ul> 
  
---
  
<div style="display: inline-block">
  <img align="center" alt="Badge-Windows" height="30px" width="40px" src="https://gitlab.com/codebuenozy/codebuenozy/-/raw/main/icons/windows.svg">
  <img align="center" alt="Badge-Linux" height="30px" width="40px" src="https://gitlab.com/codebuenozy/codebuenozy/-/raw/main/icons/linux.svg">
  <img align="center" alt="Badge-Git" height="30px" width="40px" src="https://gitlab.com/codebuenozy/codebuenozy/-/raw/main/icons/git.svg">
  <img align="center" alt="Badge-Figma" height="30px" width="40px" src="https://gitlab.com/codebuenozy/codebuenozy/-/raw/main/icons/figma.svg">
  <img align="center" alt="Badge-HTML" height="30px" width="40px" src="https://gitlab.com/codebuenozy/codebuenozy/-/raw/main/icons/html5.svg">
  <img align="center" alt="Badge-CSS" height="30px" width="40px" src="https://gitlab.com/codebuenozy/codebuenozy/-/raw/main/icons/css3.svg">
  <img align="center" alt="Badge-JS" height="30px" width="40px" src="https://gitlab.com/codebuenozy/codebuenozy/-/raw/main/icons/js.svg">
  <img align="center" alt="Badge-TS" height="30px" width="40px" src="https://gitlab.com/codebuenozy/codebuenozy/-/raw/main/icons/ts.svg">
  <img align="center" alt="Badge-NodeJS" height="30px" width="40px" src="https://gitlab.com/codebuenozy/codebuenozy/-/raw/main/icons/node.svg">
  <img align="center" alt="Badge-Npm" height="30px" width="40px" src="https://gitlab.com/codebuenozy/codebuenozy/-/raw/main/icons/npm.svg">
  <img align="center" alt="Badge-SASS" height="30px" width="40px" src="https://gitlab.com/codebuenozy/codebuenozy/-/raw/main/icons/sass.svg">
  <img align="center" alt="Badge-ReactJS" height="30px" width="40px" src="https://gitlab.com/codebuenozy/codebuenozy/-/raw/main/icons/react.svg">
  <img align="center" alt="Badge-Insomnia" height="30px" width="40px" src="https://gitlab.com/codebuenozy/codebuenozy/-/raw/main/icons/insomnia.svg">
  <img align="center" alt="Badge-MySQL" height="30px" width="40px" src="https://gitlab.com/codebuenozy/codebuenozy/-/raw/main/icons/mysql.svg">
  <img align="center" alt="Badge-MongoDB" height="30px" width="40px" src="https://gitlab.com/codebuenozy/codebuenozy/-/raw/main/icons/mongodb.svg">
</div>

---
  
<div>
  <img align="right" alt="codebuenozy-bitmoji" height="150px" style="border-radius:50px;" src="https://gitlab.com/codebuenozy/codebuenozy/-/raw/main/bitmoji/studying.png">
  <br>
  <a href ="mailto:nando.buenozy@gmail.com"><img src="https://gitlab.com/codebuenozy/codebuenozy/-/raw/main/badges/gmail.png" target="_blank"></a>
  <a href ="https://api.whatsapp.com/send?phone=5511970967963" target="_blank"><img src="https://gitlab.com/codebuenozy/codebuenozy/-/raw/main/badges/whatsapp.png" target="_blank"></a>
  <br><br><br>
</div>

---

<div align="center">
  <br>
  <a href="https://github.com/codebuenozy">
  <img height="180em" src="https://github-readme-stats.vercel.app/api?username=codebuenozy&show_icons=true&theme=dracula&include_all_commits=true&count_private=true"/>
  <img height="180em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=codebuenozy&layout=compact&langs_count=7&theme=dracula"/>   
</div>

---
